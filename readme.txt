# Instructions

1. Download & copy `./node-server` folder to your computer

2. Install node dependencies:
```sh
cd ./node-server
npm i
```

3. Run `server`
````sh
npm run start
```

4. Experiment
Click in the "Buy" button here in this stackblitz demo
to simulate a payment (it will open a stripe checkout page)

# Finish

Play around & Enjoy!

Example developed by JRuiPinto
social.jruipinto@gmail.com
https://pt.linkedin.com/in/j-rui-pinto

## Page
1. Stripe card Element
routing path: /card
simple card element provide (card number, expiry date, cvc and zipcode)

2. Checkout page
routing path: /strip-checkout
redirect to user to strip checkout page

3. payment Element
routing: /payment
payment elemt provide 
    a. Card number
    b. Expiration
    c. cvc
    d. Country
    e. zip

4. create card using diffrent element
rouing: /card-element
cardNumber element provide: Card Number
cardExpiry element provide: Card expiry
cardCvc element provide: CVC
