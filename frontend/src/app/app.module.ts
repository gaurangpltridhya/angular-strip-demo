import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';


// components
import { AppComponent } from './app.component';
import { CardComponentComponent } from './card-component/card-component.component';
import { StripCheckoutComponent } from './strip-checkout/strip-checkout.component';
import { PaymentComponent } from './payment/payment.component';
import { CardElementComponent } from './card-element/card-element.component';

@NgModule({
  declarations: [
    AppComponent,
    CardComponentComponent,
    StripCheckoutComponent,
    PaymentComponent,
    CardElementComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
