import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { loadStripe, Stripe } from '@stripe/stripe-js';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit, AfterViewInit  {
  @ViewChild('paymentElement') paymentElement!: ElementRef;
  private stripe!: Stripe;
  private paymentElementRef: any;
  stripeKey = 'pk_test_51NNrsWSE0QNSaEGNZfqiBwOwK6LTID8LDhtpVWvVrBIzcjasyRZcw2a0oRWsjDFzAAjOUKjQMttcGaEwtZXfuxIK000AdG1tme';
  clientSecret = 'pi_3Ng2YDSE0QNSaEGN08OQekgb_secret_a2g3l7VcvnmjwBYdREzIODXrr';
  card: any;
  
  constructor() { }

  ngOnInit() {
    this.getClientSecret();
  }

  ngAfterViewInit(){
    this.initializeStripe();

  }

  // Get clicent secret key
  getClientSecret(){  
    
  }

  // Load stripe uusing stripe-js
  async initializeStripe() {
    // this.stripe = await loadStripe(this.stripeKey);
      loadStripe(this.stripeKey).then((res: any) => {
      this.stripe = res; 
      this.paymentElementRef = this.paymentElement.nativeElement;

      // create payment element using clientSecret.
      const elements = this.stripe.elements({clientSecret: this.clientSecret})
      this.card = elements.create('payment');
      this.card.mount(this.paymentElementRef);
    });

    // Now you can manipulate the paymentElementRef using JavaScript
    // Example: this.paymentElementRef.style.border = '1px solid red';
  }

  // submit card
  async handlePayment(){
    try {
      const { paymentIntent, error } = await this.stripe.confirmCardPayment(this.clientSecret, {
        payment_method: {
          card: this.paymentElementRef, // Pass the PaymentElement here
          // Other payment method details...
        }
      });

      if (error) {
        console.error(error.message);
        // Handle payment error
      } else {
        // Payment succeeded
        console.log(paymentIntent);
      }
    } catch (error) {
      console.error(error);
    }
  }
}
