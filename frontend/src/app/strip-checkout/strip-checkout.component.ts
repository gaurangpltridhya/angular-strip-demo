import { Component, OnInit } from '@angular/core';
import { loadStripe, Stripe  } from '@stripe/stripe-js';

@Component({
  selector: 'app-strip-checkout',
  templateUrl: './strip-checkout.component.html',
  styleUrls: ['./strip-checkout.component.scss']
})
export class StripCheckoutComponent implements OnInit {

  stripeKey = 'pk_test_51NNrsWSE0QNSaEGNZfqiBwOwK6LTID8LDhtpVWvVrBIzcjasyRZcw2a0oRWsjDFzAAjOUKjQMttcGaEwtZXfuxIK000AdG1tme';

  constructor(){

  }

  ngOnInit(): void {
    this.initializeStripe();
  }

  // this is needed because we have to add
  // stripe script at index.html's <head>, for anti-fraud reasons
  // invoked by stripe dev team
  stripe!:Stripe;

  // Load stripe using stripe-js
  initializeStripe(){
    loadStripe(this.stripeKey).then((res: any) => {
      this.stripe = res;
    })
  }

  // click on buy button get checkout URL from Backend
  buy(id:any) {
    const data = { articleID: 12345, qty: 2 };
    console.log(JSON.stringify(data));
    // fetch node server running in localhost
    // read the README.md to know how run the server to try
    fetch("http://localhost:4242/create-checkout-session", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then((session: any) => {
        window.open(session.url)
        this.stripe.redirectToCheckout({ sessionId: session.id });
      }
      )
      .then((result: any) => {
        // If redirectToCheckout fails due to a browser or network
        // error, you should display the localized error message to your
        // customer using error.message.
        if (result.error) {
          alert(result.error.message);
        }
      })
      .catch(error => {
        console.error("Error:", error);
      });
  }

}
