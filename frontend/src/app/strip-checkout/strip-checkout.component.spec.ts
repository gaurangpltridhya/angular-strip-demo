import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StripCheckoutComponent } from './strip-checkout.component';

describe('StripCheckoutComponent', () => {
  let component: StripCheckoutComponent;
  let fixture: ComponentFixture<StripCheckoutComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StripCheckoutComponent]
    });
    fixture = TestBed.createComponent(StripCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
