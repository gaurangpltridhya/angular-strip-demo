import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardComponentComponent } from './card-component/card-component.component';
import { StripCheckoutComponent } from './strip-checkout/strip-checkout.component';
import { PaymentComponent } from './payment/payment.component';
import { CardElementComponent } from './card-element/card-element.component';


const routes: Routes = [
  {
      path: '',
      redirectTo: 'card',
      // redirectTo:'strip-checkout',
      // redirectTo:'card-element',
      pathMatch:'full'
  },
  {
    path:'card',
    component:CardComponentComponent
  },
  {
    path:'strip-checkout',
    component:StripCheckoutComponent
  },
  {
    path:'payment',
    component: PaymentComponent
  },
  {
    path:'card-element',
    component: CardElementComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
