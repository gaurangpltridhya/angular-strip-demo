import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { loadStripe, Stripe  } from '@stripe/stripe-js';

@Component({
  selector: 'app-card-element',
  templateUrl: './card-element.component.html',
  styleUrls: ['./card-element.component.scss']
})
export class CardElementComponent implements OnInit, AfterViewInit {
  @ViewChild('cardNumber') cardNumber!: ElementRef;
  @ViewChild('cardExpiry') cardExpiry!: ElementRef;
  @ViewChild('cardCvc') cardCvc!: ElementRef;

  stripeKey = 'pk_test_51NNrsWSE0QNSaEGNZfqiBwOwK6LTID8LDhtpVWvVrBIzcjasyRZcw2a0oRWsjDFzAAjOUKjQMttcGaEwtZXfuxIK000AdG1tme';
  public stripe!: Stripe
  cardObject: {card: any, expiry: any, cvc: any} = {
    card: {},
    expiry: {},
    cvc: {}
  }


  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(){
    this.initializeStripe();
  }

  // Load stripe using stripe-js
  async initializeStripe(){
    loadStripe(this.stripeKey).then((res: any) => {
      this.stripe = res;
      const cardNumberElement = this.stripe.elements();
      const cardExpiryElement = this.stripe.elements();
      const cardCvcElement = this.stripe.elements();

      // Card Number -  create card number using cardNumber element
      const cardNumber = cardNumberElement.create('cardNumber', {showIcon: true, iconStyle: 'solid'});
      cardNumber.mount(this.cardNumber.nativeElement);

      // handle error on change
      cardNumber.on('change', (event) => {
        if(event.error){
          this.cardObject.card['error'] = event.error.message;
        } else {
          this.cardObject.card['error'] = null;
        }
      })

      // Card expiry - create card expiry using cardExpiry element
      const cardExpiry = cardExpiryElement.create('cardExpiry');
      cardExpiry.mount(this.cardExpiry.nativeElement);

      // handle error on change
      cardExpiry.on('change', (event) => {
        if(event.error){
          this.cardObject.expiry['error'] = event.error.message;
        } else {
          this.cardObject.expiry['error'] = null
        }
      })

      // Card CVC - create card CVC using cardCVC element
      const cardCvc = cardCvcElement.create('cardCvc');
      cardCvc.mount(this.cardCvc.nativeElement);

      // handle error on change
      cardCvc.on('change', (event) => {
        if(event.error){
          this.cardObject.cvc['error'] = event.error.message;
        } else {
          this.cardObject.cvc['error'] = null
        }
      })
    })
  }

  handlePayment(){
    console.log(this.cardNumber.nativeElement, 'elements');

    this.stripe.confirmCardPayment("{PAYMENT_INTENT_CLIENT_SECRET}", {
      payment_method: {
        card: this.cardNumber.nativeElement,
      },
    });
  }

}

