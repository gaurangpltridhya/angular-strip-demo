import {
  Component,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef
} from '@angular/core';

import  { NgForm } from "@angular/forms"
import { AngularStripeService } from '@fireflysemantics/angular-stripe-service'

@Component({
  selector: 'app-card-component',
  templateUrl: './card-component.component.html',
  styleUrls: ['./card-component.component.scss']
})
export class CardComponentComponent {

  @ViewChild('cardInfo', { static: false }) cardInfo!: ElementRef;

  stripe:any;
  loading = false;
  confirmation:any;

  card: any;
  cardHandler = this.onChange.bind(this);
  error: any;
  cardOption: any = {
    classes: {
      base: 'card-border'
    },
    style: {
      base: {
        color: '#211f1f',
        fontWeight: '500',
        fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
        fontSize: '16px',
      },
      invalid: {
        iconColor: '#fa3939',
        color: '#fa3939',
      },
    },
    hidePostalCode: true
  }

  constructor(
    private cd: ChangeDetectorRef,
    private stripeService:AngularStripeService) {}

  // create stripe element useing AngularStripeService
  ngAfterViewInit() {
    this.stripeService.setPublishableKey('pk_test_2syov9fTMRwOxYG97AAXbOgt008X6NL46o').then(
      (stripe:any)=> {
        this.stripe = stripe;
    const elements = stripe.elements();    
    this.card = elements.create('card', this.cardOption);
    this.card.mount(this.cardInfo.nativeElement);
    this.card.addEventListener('change', this.cardHandler);
    });
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  // onchange check error
  onChange(error:any ) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  // card submit
  async onSubmit(form: NgForm) {
    console.log(this.card, 'card')
    const { token, error } = await this.stripe.createToken(this.card);

    if (error) {
      console.log('Error:', error);
      this.error = error.message
    } else {
      console.log('Success!', token);
    }
  }
  
}
