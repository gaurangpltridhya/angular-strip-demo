/**
 * 01/2021
 * Example developed by JRuiPinto
 * inspired by official docs
 *
 * If I helped you please let me now:
 * social.jruipinto@gmail.com
 * https://pt.linkedin.com/in/j-rui-pinto
 */

const stripe = require("stripe")("sk_test_4eC39HqLyjWDarjtT1zdp7dc");
const express = require("express");
const cors = require("cors");
// const { getArticleFromFakeDB } = require("./get-article-from-fakedb");
const bodyParser = require("body-parser");

const app = express();

app.use(express.static("."));
app.use(bodyParser.json());
app.use(cors());

const YOUR_DOMAIN = "http://localhost:4242";

app.post("/create-checkout-session", async (req, res) => {
  // const article = getArticleFromFakeDB(req.body.articleID);
  try {
    const stripe = require('stripe')('sk_test_51NXhQuLXh3r9RHrI4y7cjst4bF4ZwLqK1Sp8oYzvugYhhPfU6jOXwydX9j2McwrGJAR7xBURbB497cLP19cjanow00YRJrVJLE');

    const session = await stripe.checkout.sessions.create({
      success_url: 'http://localhost:4200/',
      // payment_method_types:["card", "acss_debit", "affirm", "afterpay_clearpay", "alipay", "au_becs_debit", "bacs_debit", "bancontact", "blik", "boleto", "cashapp", "customer_balance", "eps", "fpx", "giropay", "grabpay", "ideal", "klarna", "konbini", "link", "oxxo", "p24", "paynow", "paypal", "pix", "promptpay", "sepa_debit", "sofort", "us_bank_account", "wechat_pay"],
      payment_method_types:["card","alipay"],
      // create product in strip for generate URL
      line_items: [
        {price: 'price_1Ng4ruLXh3r9RHrIK8wWEsCs', quantity: 2},
      ],

      mode: 'payment',
    });
    console.log(session.url, "session")
    res.json({ id: session.id,url:session.url });
  } catch (error) {
    console.log(error.message, "error")
  }
});

app.listen(4242, () => console.log("Running on port 4242"));
